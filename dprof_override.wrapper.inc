<?php
/**
 * @file
 *
 * PHP Stream Wrapper
 */

define('DPROF_OVERRIDE_WRAPPER_LOADED', TRUE);

// -------------------- PHP STREAM WRAPPER --------------------

/**
 * Sandbox file stream wrapper.
 *
 * Custom SandboxFileStreamWrapper class that replaces / wraps the default
 * StreamWrapper of file:// streams in order to prepend the sandbox
 * namespace to Sandboxed module files containing PHP code.
 *
 * This is mostly based upon DrupalLocalStreamWrapper but handling paths
 * instead of URIs (just like the default PHP implementation does).
 *
 * Inspired by Sandbox Module ....
 */
class DprofOverrideFileStreamWrapper {

  /**
   * A generic resource handle.
   *
   * @var Resource
   */
  public $handle = NULL;

  /**
   * Support for fopen(), file_get_contents(), file_put_contents() etc.
   *
   * @param $path
   *   A string containing the path to the file to open.
   * @param $mode
   *   The file mode ("r", "wb" etc.).
   * @param $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param $opened_path
   *   A string containing the path actually opened.
   *
   * @return
   *   Returns TRUE if file was opened successfully.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-open.php
   */
  public function getPath($path) {
    $abs = FALSE;
    if (strpos($path, DRUPAL_ROOT . '/') === 0) {
      $path = substr($path, strlen(DRUPAL_ROOT) + 1);
      $abs = TRUE;
    }
    //$newpath = dirname(__FILE__) . '/overrides/' . $path;
    $newpath = variable_get('file_public_path', conf_path() . '/files') . '/dprof/' . $path;
    if (file_exists($newpath)) {
      $path = $newpath;
      //$path = substr($path, strlen(DRUPAL_ROOT) + 1);
    }
    $path = ($abs ? DRUPAL_ROOT . '/' : '') . $path;
    return $path;
  }

  public function stream_open($path, $mode) {
    // Temporarily switch file stream wrapper back to system defaults,
    // do what has to be done and switch it back to the sandboxed one.
    _dprof_restore_wrapper();
    $this->handle = fopen($this->getPath($path), $mode);
    _dprof_set_wrapper();
    return !empty($this->handle);
  }

  /**
   * Support for fread(), file_get_contents() etc.
   *
   * @param $count
   *   Maximum number of bytes to be read.
   *
   * @return
   *   The string that was read, or FALSE in case of an error.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-read.php
   */
  public function stream_read($count) {
    return fread($this->handle, $count);
  }

  /**
   * Support for fclose().
   *
   * @return
   *   TRUE if stream was successfully closed.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-close.php
   */
  public function stream_close() {
    return fclose($this->handle);
  }

  /**
   * Support for opendir().
   *
   * @param $path
   *   A string containing the path to the directory to open.
   * @param $options
   *   Unknown (parameter is not documented in PHP Manual).
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-opendir.php
   */
  public function dir_opendir($path) {
    // Temporarily switch file stream wrapper back to system defaults,
    // do what has to be done and switch it back to the sandboxed one.
    _dprof_restore_wrapper();
    $this->handle = opendir($path);
    _dprof_set_wrapper();
    return (bool) $this->handle;
  }

  /**
   * Support for stat().
   *
   * @param $path
   *   A string containing the path to get information about.
   * @param $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.url-stat.php
   */
  public function url_stat($path, $flags) {
    // Suppress warnings if requested or if the file or directory does not
    // exist. This is consistent with PHP's plain filesystem stream wrapper.
    _dprof_restore_wrapper();
    if ($flags & STREAM_URL_STAT_QUIET) {
      $stat = @stat($this->getPath($path));
    }
    else {
      $stat = stat($this->getPath($path));
    }
    _dprof_set_wrapper();
    return $stat;
  }

  /**
   * Support for fwrite(), file_put_contents() etc.
   *
   * @param $data
   *   The string to be written.
   *
   * @return
   *   The number of bytes written (integer).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-write.php
   */
  public function stream_write($data) {
    return fwrite($this->handle, $data);
  }

  /**
   * Support for ftell().
   *
   * @return
   *   The current offset in bytes from the beginning of file.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-tell.php
   */
  public function stream_tell() {
    return ftell($this->handle);
  }

  /**
   * Support for fstat().
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  public function stream_stat() {
    return fstat($this->handle);
  }
  
  /**
   * Support for feof().
   *
   * @return
   *   TRUE if end-of-file has been reached.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-eof.php
   */
  public function stream_eof() {
    return feof($this->handle);
  }

  /**
   * Support for fseek().
   *
   * @param $offset
   *   The byte offset to got to.
   * @param $whence
   *   SEEK_SET, SEEK_CUR, or SEEK_END.
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-seek.php
   */
  public function stream_seek($offset, $whence) {
    // fseek returns 0 on success and -1 on a failure.
    // stream_seek   1 on success and  0 on a failure.
    return !fseek($this->handle, $offset, $whence);
  }

  /**
   * Support for readdir().
   *
   * @return
   *   The next filename, or FALSE if there are no more files in the directory.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-readdir.php
   */
  public function dir_readdir() {
    return readdir($this->handle);
  }

  /**
   * Support for flock().
   *
   * @param $operation
   *   One of the following:
   *   - LOCK_SH to acquire a shared lock (reader).
   *   - LOCK_EX to acquire an exclusive lock (writer).
   *   - LOCK_UN to release a lock (shared or exclusive).
   *   - LOCK_NB if you don't want flock() to block while locking (not
   *     supported on Windows).
   *
   * @return
   *   Always returns TRUE at the present time.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-lock.php
   */
  public function stream_lock($operation) {
    if (in_array($operation, array(LOCK_SH, LOCK_EX, LOCK_UN, LOCK_NB))) {
      return flock($this->handle, $operation);
    }
    return TRUE;
  }

  /**
   * Support for fflush().
   *
   * @return
   *   TRUE if data was successfully stored (or there was no data to store).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-flush.php
   */
  public function stream_flush() {
    return fflush($this->handle);
  }
  
  /**
   * Support for unlink().
   *
   * @param $path
   *   A string containing the path to the resource to delete.
   *
   * @return
   *   TRUE if resource was successfully deleted.
   *
   * @see http://php.net/manual/en/streamwrapper.unlink.php
   */
  public function unlink($path) {
    // Temporarily switch file stream wrapper back to system defaults,
    // do what has to be done and switch it back to the sandboxed one.
    _dprof_restore_wrapper();
    $result = drupal_unlink($this->getPath($path));
    _dprof_set_wrapper();
    return $result;
  }
  
  /**
   * Support for rename().
   *
   * @param $from_path,
   *   The path to the file to rename.
   * @param $to_path
   *   The new path for file.
   *
   * @return
   *   TRUE if file was successfully renamed.
   *
   * @see http://php.net/manual/en/streamwrapper.rename.php
   */
  public function rename($from_path, $to_path) {
    _dprof_restore_wrapper();
    $result = rename($this->getPath($from_path), $this->getPath($to_path));
    _dprof_set_wrapper();
  }
  
  /**
   * Support for mkdir().
   *
   * @param $path
   *   A string containing the path to the directory to create.
   * @param $mode
   *   Permission flags - see mkdir().
   * @param $options
   *   A bit mask of STREAM_REPORT_ERRORS and STREAM_MKDIR_RECURSIVE.
   *
   * @return
   *   TRUE if directory was successfully created.
   *
   * @see http://php.net/manual/en/streamwrapper.mkdir.php
   */
  public function mkdir($path, $mode, $options) {
    _dprof_restore_wrapper();
    $recursive = (bool) ($options & STREAM_MKDIR_RECURSIVE);
    if ($options & STREAM_REPORT_ERRORS) {
      $result = mkdir($path, $mode, $recursive);
    }
    else {
      $result = @mkdir($path, $mode, $recursive);
    }
    _dprof_set_wrapper();
    return $result;
  }

  /**
   * Support for rmdir().
   *
   * @param $path
   *   A string containing the path to the directory to delete.
   * @param $options
   *   A bit mask of STREAM_REPORT_ERRORS.
   *
   * @return
   *   TRUE if directory was successfully removed.
   *
   * @see http://php.net/manual/en/streamwrapper.rmdir.php
   */
  public function rmdir($path, $options) {
    // Temporarily switch file stream wrapper back to system defaults,
    // do what has to be done and switch it back to the sandboxed one.
    _dprof_restore_wrapper();
    if ($options & STREAM_REPORT_ERRORS) {
      $result = drupal_rmdir($path);
    }
    else {
      $result = @drupal_rmdir($path);
    }
    _dprof_set_wrapper();
    return $result;
  }

  /**
   * Support for rewinddir().
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-rewinddir.php
   */
  public function dir_rewinddir() {
    rewinddir($this->handle);
    // We do not really have a way to signal a failure as rewinddir() does not
    // have a return value and there is no way to read a directory handler
    // without advancing to the next file.
    return TRUE;
  }

  /**
   * Support for closedir().
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-closedir.php
   */
  public function dir_closedir() {
    closedir($this->handle);
    // We do not really have a way to signal a failure as closedir() does not
    // have a return value.
    return TRUE;
  }

}


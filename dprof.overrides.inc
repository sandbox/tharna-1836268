<?php

function theme($hook, $variables = array()) {
  if (is_array($hook)) {
    foreach ($hook as $candidate) {
      if (isset($hooks[$candidate])) {
        break;
      }
    }
    $hook = $candidate;
  }

  dprof_start('theme::'. $hook);
  dprof_start('system::theme');
  $return = drupal_core_theme($hook, $variables); 
  //$return = call_user_func_array( 'drupal_core_theme', func_get_args( )); 
  dprof_stop('theme::'. $hook);
  dprof_stop('system::theme');
  return $return;
}

function entity_load($entity_type, $ids = FALSE, $conditions = array(), $reset = FALSE) {
  dprof_start('entity::'. $entity_type);
  $return = drupal_core_entity_load($entity_type, $ids, $conditions, $reset);
  dprof_stop('entity::'. $entity_type);
  return $return;
}

function module_invoke($module, $hook) {
  dprof_start('module::'. $module);
  dprof_start('hook::'. $module);
  $return = call_user_func_array( 'drupal_core_module_invoke', func_get_args( )); 
  dprof_stop('module::'. $module);
  dprof_stop('hook::'. $module);
  return $return;
}

// TODO handle core version differences somehow
function module_invoke_all($hook){
  dprof_start('hook::'. $hook);
  $args = func_get_args();
  // Remove $hook from the arguments.
  unset($args[0]);
  $return = array();
  foreach (module_implements($hook) as $module) {
    dprof_start('module::'. $module);
    $function = $module . '_' . $hook;
    if (function_exists($function)) {
      $result = call_user_func_array($function, $args);
      if (isset($result) && is_array($result)) {
        $return = array_merge_recursive($return, $result);
      }
      elseif (isset($result)) {
        $return[] = $result;
      }
    }
    dprof_stop('module::'. $module);
  }

  dprof_stop('hook::'. $hook);
  return $return;
}

function _block_render_blocks($region_blocks) {
  // Block caching is not compatible with node access modules. We also
  // preserve the submission of forms in blocks, by fetching from cache only
  // if the request method is 'GET' (or 'HEAD').
  $cacheable = !count(module_implements('node_grants')) && ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'HEAD');
  foreach ($region_blocks as $key => $block) {
    dprof_start('block::'. $block->module .'-'. $block->delta);
    // Render the block content if it has not been created already.
    if (!isset($block->content)) {
      // Erase the block from the static array - we'll put it back if it has
      // content.
      unset($region_blocks[$key]);
      // Try fetching the block from cache.
      if ($cacheable && ($cid = _block_get_cache_id($block)) && ($cache = cache_get($cid, 'cache_block'))) {
        $array = $cache->data;
      }
      else {
        $array = module_invoke($block->module, 'block_view', $block->delta);

        // Allow modules to modify the block before it is viewed, via either
        // hook_block_view_alter() or hook_block_view_MODULE_DELTA_alter().
        drupal_alter(array('block_view', "block_view_{$block->module}_{$block->delta}"), $array, $block);

        if (isset($cid)) {
          cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
        }
      }

      if (isset($array) && is_array($array)) {
        foreach ($array as $k => $v) {
          $block->$k = $v;
        }
      }
      if (isset($block->content) && $block->content) {
        // Normalize to the drupal_render() structure.
        if (is_string($block->content)) {
          $block->content = array('#markup' => $block->content);
        }
        // Override default block title if a custom display title is present.
        if ($block->title) {
          // Check plain here to allow module generated titles to keep any
          // markup.
          $block->subject = $block->title == '<none>' ? '' : check_plain($block->title);
        }
        if (!isset($block->subject)) {
          $block->subject = '';
        }
        $region_blocks["{$block->module}_{$block->delta}"] = $block;
      }
    }
    dprof_stop('block::'. $block->module .'-'. $block->delta);
  }
  return $region_blocks;
}

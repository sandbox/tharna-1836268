<?php
/**
 * Implements hook_drush_command().
 */
function dprof_drush_command() {
  $items['dprof-rebuild'] = array(
    'description' => dt('Rebuild dprof overrides.'),
  );
  return $items ;
}
function drush_dprof_rebuild( ) {
  _dprof_set_wrapper( );
  dprof_create_overrides( );
  _dprof_restore_wrapper( );
}

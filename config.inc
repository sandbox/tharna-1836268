<?php
// Don't mess with drush
if(function_exists('drupal_is_cli') && drupal_is_cli()){
  return;
}

include dirname(__FILE__) . '/dprof_override.wrapper.inc';
if(!function_exists('module_invoke')){
  include dirname(__FILE__) . '/dprof.overrides.inc';
} else {
  // TODO handle errors
}
_dprof_set_wrapper();

/**
 * Hijack files
 */
function _dprof_set_wrapper() {
  if (!defined('DPROF_OVERRIDE_WRAPPER_LOADED')) {
    return;
  }
  stream_wrapper_unregister('file');
  stream_wrapper_register('file', 'DprofOverrideFileStreamWrapper');
}

/**
 * Un-hijack files
 */
function _dprof_restore_wrapper() {
  if (!defined('DPROF_OVERRIDE_WRAPPER_LOADED')) {
    return;
  }
  stream_wrapper_restore('file');
}

